package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/pkg/errors"

	"github.com/boltdb/bolt"
)

// ErrUserNotFound ...
type ErrUserNotFound error

type storage struct {
	db *bolt.DB
}

func newStorage() (storage, func()) {
	boltDB, err := bolt.Open("subscribegun.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	if err := boltDB.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("users"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	}); err != nil {
		log.Fatal(err)
	}
	return storage{db: boltDB}, func() { boltDB.Close() }
}

func (storage *storage) addUser(usr user) error {
	if _, err := storage.getUser(usr.UUID); err != nil {
		switch err := errors.Cause(err).(type) {
		case ErrUserNotFound:
			// user does not exist, continue
		default:
			return err
		}
	}
	return storage.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("users"))
		bytes, err := json.Marshal(usr)
		if err != nil {
			return err
		}
		return b.Put([]byte(usr.UUID), bytes)
	})
}

func (storage *storage) updateUser(usr user) error {
	return storage.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("users"))
		bytes, err := json.Marshal(usr)
		if err != nil {
			return err
		}
		return b.Put([]byte(usr.UUID), bytes)
	})
}

func (storage *storage) getUser(uuid string) (user, error) {
	var usr user
	err := storage.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("users"))
		v := b.Get([]byte(uuid))
		if v == nil {
			return ErrUserNotFound(errors.New("user not found"))
		}
		return json.Unmarshal(v, &usr)
	})
	return usr, err
}

func (storage *storage) deleteUser(uuid string) error {
	return storage.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("users"))
		return b.Delete([]byte(uuid))
	})
}
