package main

import (
	"errors"
	"fmt"
	"html/template"
	"strconv"
	"time"
)

var funcMap = template.FuncMap{
	"string": func(value interface{}) string {
		switch v := value.(type) {
		case string:
			return v
		case int:
			return strconv.Itoa(v)
		case []byte:
			return string(v)
		case time.Time:
			return time.Time(v).Format("02.01.2006 15:04:05")
		// case mailgun.TimestampNano:
		// 	return time.Time(v).Format("02.01.2006 15:04:05")
		default:
			return ""
		}
	},
	"derefBool": func(i *bool) bool { return *i },
	"html": func(value interface{}) template.HTML {
		return template.HTML(fmt.Sprint(value))
	},
}

func getTemplate(tmpl ...string) (*template.Template, error) {
	if len(tmpl) == 0 {
		return nil, errors.New("provide at leas one template")
	}
	main := MustAsset("templates/" + tmpl[0] + ".html")
	mainTmpl, err := template.New(tmpl[0]).Funcs(funcMap).Parse(string(main))
	if err != nil {
		return nil, err
	}
	for _, t := range tmpl[1:] {
		mainTmpl, err = mainTmpl.Parse(string(MustAsset("templates/" + t + ".html")))
		if err != nil {
			return nil, err
		}
	}
	mainTmpl.Funcs(funcMap)
	return mainTmpl, err
}
