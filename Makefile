help:
	@echo "dev				- run locally with templates built for dev"
	@echo "build-container	- builds docker image"
	@echo "run-container 	- run previously built container"
	@echo "templates		- compile templates for prod"
dev:
	go-bindata -debug templates
	SUBSCRIBEGUN_URL="" \
	gin run main.go

templates:
	go-bindata templates

build-container: templates
	(cd frontend; npm run build)
	GOOS=linux go build
	docker build -t subscribegun .
	rm subscribegun

run-container:
	docker run -ti --rm -e PORT=3030 -p 3030:3030 subscribegun:latest

deploy:
	docker pull registry.gitlab.com/miloskrca/subscribegun 
	docker tag registry.gitlab.com/miloskrca/subscribegun registry.heroku.com/subscribegun/web
	docker push registry.heroku.com/subscribegun/web
	heroku container:release web --app subscribegun

.PHONY: dev templates build-container run-container deploy