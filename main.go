package main

import (
	"html/template"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}
	// subscribegunURL := os.Getenv("SUBSCRIBEGUN_URL")
	// var cookieHandler = securecookie.New(
	// 	securecookie.GenerateRandomKey(64),
	// 	securecookie.GenerateRandomKey(32),
	// )
	storage, close := newStorage()
	defer close()
	// cookieStore := sessions.NewCookieStore([]byte("subscribegun"))
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(60 * time.Second))
	r.Mount("/subscribe", subscribeResource{storage}.routes())
	r.Mount("/api", apiResource{storage}.routes())
	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		tmpl := template.Must(template.ParseFiles("frontend/dist/index.html"))
		tmpl.Execute(w, nil)
	})
	r.Mount("/js/", http.FileServer(http.Dir("frontend/dist")))
	r.Mount("/css/", http.FileServer(http.Dir("frontend/dist")))
	log.Println("Running on " + port + "...")
	if err := http.ListenAndServe(":"+port, r); err != nil {
		log.Fatal(err)
	}
}
