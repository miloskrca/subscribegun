FROM alpine

RUN apk --update add ca-certificates
RUN mkdir -p frontend/dist

COPY subscribegun /
COPY templates /
COPY frontend/dist /frontend/dist

CMD /subscribegun