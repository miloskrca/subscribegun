import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'

if (process.env.NODE_ENV == "development") {
  axios.defaults.baseURL = 'http://localhost:3000';
} else {
  axios.defaults.baseURL = window.location.protocol + '//' + window.location.hostname
    + (window.location.port !== "" ? ":" + window.location.port : "");
}

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
