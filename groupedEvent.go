package main

import (
	"context"
	"encoding/json"
	"log"
	"sort"
	"strings"
	"time"

	mailgun "github.com/mailgun/mailgun-go/v4"
	"github.com/mailgun/mailgun-go/v4/events"
)

type gEvent struct {
	Recipients []string  `json:"recipients"`
	Subject    string    `json:"subject"`
	MessageID  string    `json:"msgID"`
	Time       time.Time `json:"time"`
	Events     []sEvent  `json:"events"`
	Hash       string    `json:"hash"`
	Tags       []string  `json:"tags"`
	StorageURL string    `json:"storageURL"`
}

func (e *gEvent) MarshalJSON() ([]byte, error) {
	type Alias gEvent
	return json.Marshal(&struct {
		*Alias
		Time string `json:"time"`
	}{
		Alias: (*Alias)(e),
		Time:  e.Time.Format(time.RFC822),
	})
}

// type safeClientInfo struct {
// 	ClientType mailgun.ClientType `json:"client-type,omitempty"`
// 	ClientOS   string             `json:"client-os,omitempty"`
// 	ClientName string             `json:"client-name,omitempty"`
// 	DeviceType mailgun.DeviceType `json:"device-type,omitempty"`
// 	UserAgent  string             `json:"user-agent,omitempty"`
// }

type sEvent struct {
	MessageID  string            `json:"messageID"`
	Recipient  string            `json:"recipient"`
	Event      string            `json:"event"`
	Time       time.Time         `json:"time"`
	URL        string            `json:"url"`
	ClientInfo events.ClientInfo `json:"clientInfo"`
	Tags       []string          `json:"tags"`
	Subject    string            `json:"subject"`
	StorageURL string            `json:"storageURL"`
}

func (e *sEvent) MarshalJSON() ([]byte, error) {
	type Alias sEvent
	return json.Marshal(&struct {
		*Alias
		Time string `json:"time"`
	}{
		Alias: (*Alias)(e),
		Time:  e.Time.Format(time.RFC822),
	})
}

func newSEvent(e mailgun.Event, now time.Time) (sEvent, error) {
	var mesage events.Message
	var eventType string
	var storageURL string
	var subject string
	var tags []string
	var clientInfo = events.ClientInfo{}
	var url string
	var recipient string
	var messageID string

	switch event := e.(type) {
	case *events.Accepted:
		// fmt.Printf("Accepted: auth: %t\n", event.Flags.IsAuthenticated)
		mesage = event.Message
		messageID = mesage.Headers.MessageID
		subject = mesage.Headers.Subject
		recipient = event.Recipient
		eventType = "Accepted"
		messageID = mesage.Headers.MessageID
		// only events in last 3 days have the content stored
		storageURL = event.Storage.URL
		tags = event.Tags
	case *events.Delivered:
		// fmt.Printf("Delivered transport: %s\n", event.Envelope.Transport)
		mesage = event.Message
		messageID = mesage.Headers.MessageID
		recipient = event.Recipient
		eventType = "Delivered"
		subject = mesage.Headers.Subject
		// only events in last 3 days have the content stored
		storageURL = event.Storage.URL
		tags = event.Tags
	case *events.Failed:
		// fmt.Printf("Failed reason: %s\n", event.Reason)
		mesage = event.Message
		messageID = mesage.Headers.MessageID
		recipient = event.Recipient
		eventType = "Failed"
		storageURL = event.Storage.URL
		subject = mesage.Headers.Subject
		tags = event.Tags
	case *events.Clicked:
		// fmt.Printf("Clicked GeoLocation: %s\n", event.GeoLocation.Country)
		mesage = event.Message
		messageID = mesage.Headers.MessageID
		recipient = event.Recipient
		eventType = "Clicked"
		subject = mesage.Headers.Subject
		tags = event.Tags
		clientInfo = event.ClientInfo
		url = event.Url
	case *events.Opened:
		// fmt.Printf("Opened GeoLocation: %s\n", event.GeoLocation.Country)
		mesage = event.Message
		messageID = mesage.Headers.MessageID
		recipient = event.Recipient
		eventType = "Opened"
		subject = mesage.Headers.Subject
		tags = event.Tags
		clientInfo = event.ClientInfo
	case *events.Rejected:
		// fmt.Printf("Rejected reason: %s\n", event.Reject.Reason)
		mesage = event.Message
		messageID = mesage.Headers.MessageID
		eventType = "Rejected"
		storageURL = event.Storage.URL
		subject = mesage.Headers.Subject
		tags = event.Tags
	case *events.Stored:
		// fmt.Printf("Stored URL: %s\n", event.Storage.URL)
		mesage = event.Message
		messageID = mesage.Headers.MessageID
		eventType = "Stored"
		storageURL = event.Storage.URL
		subject = mesage.Headers.Subject
		tags = event.Tags
	case *events.Unsubscribed:
		// fmt.Printf("Unsubscribed client OS: %s\n", event.ClientInfo.ClientOS)
		mesage = event.Message
		messageID = mesage.Headers.MessageID
		recipient = event.Recipient
		eventType = "Unsubscribed"
		subject = mesage.Headers.Subject
		tags = event.Tags
		clientInfo = event.ClientInfo
	}

	return sEvent{
		MessageID:  messageID,                   // accepted, delivered, opened, clicked
		Recipient:  recipient,                   // accepted, delivered, opened, clicked
		Event:      eventType,                   // accepted, delivered, opened, clicked
		Time:       time.Time(e.GetTimestamp()), // accepted, delivered, opened, clicked
		ClientInfo: clientInfo,                  // opened, clicked
		URL:        url,                         // clicked
		Tags:       tags,                        // accepted, delivered, opened, clicked
		Subject:    subject,                     // delivered, accepted
		StorageURL: storageURL,
	}, nil
}

func getGroupedEvents(ctx context.Context, domain, list string, u user, filter map[string]string) []gEvent {
	now := time.Now()
	begin := now.Add(-30 * 24 * time.Hour)
	end := now
	if list != "" {
		filter["list"] = list
	}
	groupedEvents := make(map[string]gEvent)
	mg := mailgun.NewMailgun(domain, u.APIKey)
	it := mg.ListEvents(&mailgun.ListEventOptions{
		Compact: true,
		Begin:   begin,
		End:     end,
		Filter:  filter,
	})
	var page []mailgun.Event
	for it.Next(ctx, &page) {
		for _, event := range page {
			singleEvent, err := newSEvent(event, now)
			if err != nil {
				log.Printf("could not create single event: %v", err)
				continue
			}
			if strings.HasPrefix(singleEvent.Event, "list_") {
				// skipping list events
				continue
			}
			if groupedEvent, found := groupedEvents[singleEvent.MessageID]; found {
				groupedEvent.Events = append([]sEvent{singleEvent}, groupedEvent.Events...)
				groupedEvent.Tags = removeDuplicates(append(groupedEvent.Tags, singleEvent.Tags...))
				if singleEvent.Time.After(groupedEvent.Time) {
					groupedEvent.Time = singleEvent.Time
				}
				if groupedEvent.Subject == "" && singleEvent.Subject != "" {
					groupedEvent.Subject = singleEvent.Subject
				}
				if groupedEvent.StorageURL == "" && singleEvent.StorageURL != "" {
					groupedEvent.StorageURL = singleEvent.StorageURL
				}
				// more recipients?
				found := false
				for _, r := range groupedEvent.Recipients {
					if singleEvent.Recipient == r {
						found = true
					}
				}
				if !found {
					groupedEvent.Recipients = append(groupedEvent.Recipients, singleEvent.Recipient)
				}
				groupedEvents[singleEvent.MessageID] = groupedEvent
			} else {
				groupedEvents[singleEvent.MessageID] = gEvent{
					Hash:       singleEvent.MessageID,
					Subject:    singleEvent.Subject,
					Time:       singleEvent.Time,
					Recipients: []string{singleEvent.Recipient},
					MessageID:  singleEvent.MessageID,
					Events:     []sEvent{singleEvent},
					Tags:       singleEvent.Tags,
					StorageURL: singleEvent.StorageURL,
				}
			}
		}
	}
	values := []gEvent{}
	if it.Err() != nil {
		log.Printf("could not iterate: %v", it.Err())
		return values
	}
	for _, v := range groupedEvents {
		values = append(values, v)
	}
	sort.Sort(byTime(values))
	return values
}

type byTime []gEvent

func (s byTime) Len() int {
	return len(s)
}
func (s byTime) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byTime) Less(i, j int) bool {
	timeOne := s[i].Time
	timeTwo := s[j].Time
	return time.Time(timeOne).After(time.Time(timeTwo))
}

func removeDuplicates(elements []string) []string {
	// Use map to record duplicates as we find them.
	encountered := map[string]bool{}
	result := []string{}

	for v := range elements {
		if encountered[elements[v]] == true {
			// Do not add duplicate.
		} else {
			// Record this element as an encountered element.
			encountered[elements[v]] = true
			// Append to result slice.
			result = append(result, elements[v])
		}
	}
	// Return the new slice.
	return result
}
