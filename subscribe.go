package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/avct/uasurfer"
	"github.com/go-chi/chi"
	mailgun "github.com/mailgun/mailgun-go/v4"
)

type subscribeResource struct {
	db storage
}

func (sr subscribeResource) routes() chi.Router {
	r := chi.NewRouter()
	r.Post("/", sr.index)
	return r
}

// type GeoIP struct {
// 	// The right side is the name of the JSON variable
// 	Ip          string  `json:"ip"`
// 	CountryCode string  `json:"country_code"`
// 	CountryName string  `json:"country_name""`
// 	RegionCode  string  `json:"region_code"`
// 	RegionName  string  `json:"region_name"`
// 	City        string  `json:"city"`
// 	Zipcode     string  `json:"zipcode"`
// 	Lat         float32 `json:"latitude"`
// 	Lon         float32 `json:"longitude"`
// 	MetroCode   int     `json:"metro_code"`
// 	AreaCode    int     `json:"area_code"`
// }

// address := r.Header.Get("X-Forwarded-For")
// // Use freegeoip.net to get a JSON response
// // There is also /xml/ and /csv/ formats available
// response, err := http.Get("https://freegeoip.net/json/" + address)
// if err != nil {
// 	fmt.Println(err)
// }
// defer response.Body.Close()

// // response.Body() is a reader type. We have
// // to use ioutil.ReadAll() to read the data
// // in to a byte slice(string)
// body, err := ioutil.ReadAll(response.Body)
// if err != nil {
// 	fmt.Println(err)
// }
// var geo GeoIP
// // Unmarshal the JSON byte slice to a GeoIP struct
// err = json.Unmarshal(body, &geo)
// if err != nil {
// 	fmt.Println(err)
// }

// // Everything accessible in struct now
// fmt.Println("\n==== IP Geolocation Info ====\n")
// fmt.Println("IP address:\t", geo.Ip)
// fmt.Println("Country Code:\t", geo.CountryCode)
// fmt.Println("Country Name:\t", geo.CountryName)
// fmt.Println("Zip Code:\t", geo.Zipcode)
// fmt.Println("Latitude:\t", geo.Lat)
// fmt.Println("Longitude:\t", geo.Lon)
// fmt.Println("Metro Code:\t", geo.MetroCode)
// fmt.Println("Area Code:\t", geo.AreaCode)
// return

func (sr subscribeResource) index(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	list := r.Form.Get("list")
	email := r.Form.Get("email")
	uuid := r.Form.Get("token")
	redirect := r.Form.Get("redirect")
	u, err := sr.db.getUser(uuid)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusFailedDependency)
		return
	}
	if list == "" {
		http.Error(w, "list empty", http.StatusBadRequest)
		return
	}
	if email == "" {
		http.Error(w, "email empty", http.StatusBadRequest)
		return
	}
	components := strings.Split(list, "@")
	domain := components[1]
	referrer, err := url.Parse(r.Referer())
	if err != nil {
		http.Error(w, "referer missformatted", http.StatusBadRequest)
		return
	}
	userAgent := uasurfer.Parse(r.Header.Get("User-Agent"))
	mg := mailgun.NewMailgun(domain, u.APIKey)
	errMg := mg.CreateMember(r.Context(), false, list, mailgun.Member{
		Address:    email,
		Subscribed: mailgun.Subscribed,
		Vars: map[string]interface{}{
			"timestamp":           fmt.Sprintf("%d", time.Now().Unix()),
			"subscription_source": fmt.Sprintf("%s://%s", referrer.Scheme, referrer.Host),
			"device":              userAgent.DeviceType.String(),
			"browser":             userAgent.Browser.Name.String(),
			"browser_version":     fmt.Sprintf("%d.%d.%d", userAgent.Browser.Version.Major, userAgent.Browser.Version.Minor, userAgent.Browser.Version.Patch),
			"os":                  userAgent.OS.Name.String(),
			"os_platform":         userAgent.OS.Platform.String(),
			"os_version":          fmt.Sprintf("%d.%d.%d", userAgent.OS.Version.Major, userAgent.OS.Version.Minor, userAgent.OS.Version.Patch),
		},
	})
	if redirect != "" {
		http.Redirect(w, r, redirect, http.StatusSeeOther)
		return
	}
	tmpl, err := getTemplate("subscribed")
	if err != nil {
		log.Fatal(err)
	}
	if err := tmpl.Execute(w, struct {
		Error error
		List  string
	}{
		Error: errMg,
		List:  list,
	}); err != nil {
		log.Fatal(err)
	}
}
