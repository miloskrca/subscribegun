package main

import (
	"bytes"
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/pkg/errors"

	"github.com/go-chi/chi"
	mailgun "github.com/mailgun/mailgun-go/v4"
	"github.com/mailgun/mailgun-go/v4/events"
)

type apiResource struct {
	db storage
}

func (ar apiResource) routes() chi.Router {
	r := chi.NewRouter()
	r.Use(corsHandler)
	r.Post("/login", ar.login)
	r.Post("/domains", authentication(ar.domains))
	r.Post("/lists", authentication(ar.lists))
	r.Post("/lists/new", authentication(ar.newList))
	r.Post("/members", authentication(ar.members))
	r.Post("/members/new", authentication(ar.newMember))
	r.Post("/members/delete", authentication(ar.deleteMember))
	r.Post("/events", authentication(ar.events))
	r.Post("/message/html", authentication(ar.storageHTML))
	r.Post("/toggleSubscribe", authentication(ar.toggleSubscribe))
	r.Post("/send", authentication(ar.send))
	r.Post("/template", authentication(ar.template))
	r.Post("/statistics", authentication(ar.statistics))
	r.Post("/saveAsTemplate", authentication(ar.saveAsTemplate))
	r.Post("/loadTemplate", authentication(ar.loadTemplate))
	r.Mount("/oauth", newDropboxResource().routes())
	return r
}

func (ar apiResource) login(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	var b struct {
		APIKey string `json:"apiKey"`
	}
	getBody(r, &b)
	checksum := md5.Sum([]byte(b.APIKey))
	uuid := hex.EncodeToString(checksum[:])
	u, err := ar.db.getUser(uuid)
	if err != nil {
		switch err := errors.Cause(err).(type) {
		case ErrUserNotFound:
			u = user{APIKey: b.APIKey, UUID: uuid}
			if err := ar.db.addUser(u); err != nil {
				renderJSON(w, http.StatusInternalServerError, err)
				return
			}
			renderJSON(w, http.StatusCreated, u)
			return
		default:
			renderJSON(w, http.StatusInternalServerError, err)
			return
		}
	}
	if u.APIKey != b.APIKey {
		renderJSON(w, http.StatusUnauthorized, errors.New("apiKeys did not match"))
		return
	}
	renderJSON(w, http.StatusOK, u)
}

func (ar apiResource) domains(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string `json:"apiKey"`
	}
	getBody(r, &b)
	renderJSON(w, http.StatusOK, getDomains(r.Context(), user{
		APIKey: b.APIKey,
	}))
}

func (ar apiResource) lists(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string `json:"apiKey"`
		Domain string `json:"domain"`
	}
	getBody(r, &b)
	renderJSON(w, http.StatusOK, getLists(
		r.Context(),
		b.Domain,
		user{APIKey: b.APIKey},
	))
}

func (ar apiResource) newList(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string `json:"apiKey"`
		Data   struct {
			Domain  string `json:"domain"`
			Address string `json:"address"`
			Name    string `json:"name"`
		} `json:"data"`
	}
	getBody(r, &b)
	mg := mailgun.NewMailgun(b.Data.Domain, b.APIKey)
	list, err := mg.CreateMailingList(r.Context(), mailgun.MailingList{
		Address:     b.Data.Address + "@" + b.Data.Domain,
		Name:        b.Data.Name,
		AccessLevel: mailgun.AccessLevelReadOnly,
	})
	if err != nil {
		renderJSON(w, http.StatusFailedDependency, err)
		return
	}
	renderJSON(w, http.StatusOK, list)
}

func (ar apiResource) members(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string `json:"apiKey"`
		List   string `json:"listAddress"`
	}
	getBody(r, &b)
	renderJSON(w, http.StatusOK, getMembers(
		r.Context(),
		user{
			APIKey: b.APIKey,
		},
		b.List,
	))
}

func (ar apiResource) deleteMember(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string `json:"apiKey"`
		List   string `json:"listAddress"`
		Email  string `json:"email"`
	}
	getBody(r, &b)
	mg := mailgun.NewMailgun("", b.APIKey)
	if err := mg.DeleteMember(r.Context(), b.Email, b.List); err != nil {
		renderJSON(w, http.StatusFailedDependency, err)
		return
	}
	renderJSON(w, http.StatusOK, nil)
}

func (ar apiResource) newMember(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string `json:"apiKey"`
		Data   struct {
			List  string `json:"listAddress"`
			Name  string `json:"name"`
			Email string `json:"email"`
		} `json:"data"`
	}
	getBody(r, &b)
	domain := getDomain(b.Data.List)
	mg := mailgun.NewMailgun(domain, b.APIKey)
	if err := mg.CreateMember(r.Context(), false, b.Data.List, mailgun.Member{
		Address:    b.Data.Email,
		Subscribed: mailgun.Subscribed,
		Name:       b.Data.Name,
		Vars: map[string]interface{}{
			"subscription_source": r.Referer(),
		},
	}); err != nil {
		renderJSON(w, http.StatusFailedDependency, err)
		return
	}
	renderJSON(w, http.StatusOK, nil)
}

func (ar apiResource) storageHTML(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey     string `json:"apiKey"`
		StorageURL string `json:"storageURL"`
		Domain     string `json:"domain"`
	}
	getBody(r, &b)
	if b.StorageURL == "" {
		renderJSON(w, http.StatusUnprocessableEntity, nil)
		return
	}
	domain := getDomain(b.Domain)
	mg := mailgun.NewMailgun(domain, b.APIKey)
	msg, err := mg.GetStoredMessage(r.Context(), b.StorageURL)
	if err != nil {
		renderJSON(w, http.StatusFailedDependency, err)
		return
	}
	renderJSON(w, http.StatusOK, msg.BodyHtml)
}

func (ar apiResource) events(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string            `json:"apiKey"`
		List   string            `json:"listAddress"`
		Domain string            `json:"domain"`
		Filter map[string]string `json:"filter"`
	}
	getBody(r, &b)
	domain := b.Domain
	if b.Domain == "" {
		domain = getDomain(b.List)
	}
	renderJSON(w, http.StatusOK, getGroupedEvents(
		r.Context(),
		domain,
		b.List,
		user{
			APIKey: b.APIKey,
		},
		b.Filter,
	))
}

func (ar apiResource) toggleSubscribe(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey       string `json:"apiKey"`
		List         string `json:"listAddress"`
		Name         string `json:"name"`
		Email        string `json:"email"`
		IsSubscribed bool   `json:"isSubscribed"`
	}
	getBody(r, &b)
	domain := getDomain(b.List)
	mg := mailgun.NewMailgun(domain, b.APIKey)
	member, err := mg.GetMember(r.Context(), b.Email, b.List)
	if err != nil {
		renderJSON(w, http.StatusFailedDependency, err)
		return
	}
	vars := member.Vars
	vars["subscription_source"] = fmt.Sprintf("subscribegun by %s", r.Referer())
	vars["subscription_edited_manually"] = true
	member, err = mg.UpdateMember(r.Context(), b.Email, b.List, mailgun.Member{
		Address:    b.Email,
		Name:       b.Name,
		Subscribed: &b.IsSubscribed,
		Vars:       vars,
	})
	if err != nil {
		renderJSON(w, http.StatusFailedDependency, err)
		return
	}
	renderJSON(w, http.StatusOK, member)
}

type clientInfo struct {
	ClientType map[string]int `json:"client-type"`
	ClientOS   map[string]int `json:"client-os"`
	ClientName map[string]int `json:"client-name"`
	DeviceType map[string]int `json:"device-type"`
	// UserAgent  map[string]int `json:"user-agent"`
}
type statistic struct {
	Total *clientInfo `json:"total"`
	// Events     map[mailgun.EventType]clientInfo `json:"events"`
	EventCount map[string]int `json:"eventCount"`
}

func (ar apiResource) statistics(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string `json:"apiKey"`
		List   string `json:"listAddress"`
		Domain string `json:"domain"`
	}
	getBody(r, &b)
	domain := getDomain(b.List)
	if b.Domain != "" {
		domain = b.Domain
	}
	s := statistic{
		// Events: map[mailgun.EventType]clientInfo{},
	}
	allEvents := getEvents(r.Context(), domain, b.List, user{APIKey: b.APIKey})
	if len(allEvents) > 0 {
		if s.EventCount == nil {
			s.EventCount = map[string]int{}
		}
	}
	for _, event := range allEvents {
		s.EventCount[event.Event]++
		if event.Event != events.EventOpened && event.Event != events.EventClicked {
			continue
		}
		if s.Total == nil {
			s.Total = &clientInfo{
				ClientName: map[string]int{},
				ClientOS:   map[string]int{},
				ClientType: map[string]int{},
				DeviceType: map[string]int{},
			}
		}
		// if _, ok := s.Events[event.Event]; !ok {
		// 	s.Events[event.Event] = clientInfo{
		// 		ClientName: map[string]int{},
		// 		ClientOS:   map[string]int{},
		// 		ClientType: map[string]int{},
		// 		DeviceType: map[string]int{},
		// 		// UserAgent:  map[string]int{},
		// 	}
		// }
		// s.Events[event.Event].ClientName[*event.ClientInfo.ClientName]++
		s.Total.ClientName[event.ClientInfo.ClientName]++
		// s.Events[event.Event].ClientOS[*event.ClientInfo.ClientOS]++
		s.Total.ClientOS[event.ClientInfo.ClientOS]++
		// s.Events[event.Event].ClientType[event.ClientInfo.ClientType.String()]++
		s.Total.ClientType[event.ClientInfo.ClientType]++
		// s.Events[event.Event].DeviceType[event.ClientInfo.DeviceType.String()]++
		s.Total.DeviceType[event.ClientInfo.DeviceType]++
		// s[event.Event].UserAgent[*event.ClientInfo.UserAgent]++
	}
	renderJSON(w, http.StatusOK, s)
}

func (ar apiResource) send(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey string `json:"apiKey"`
		Data   struct {
			FromName  string   `json:"fromName"`
			FromEmail string   `json:"fromEmail"`
			Subject   string   `json:"subject"`
			Body      string   `json:"body"`
			HTMLBody  string   `json:"htmlBody"`
			ToEmail   string   `json:"toEmail"`
			Domain    string   `json:"domain"`
			Tags      []string `json:"tags"`
		} `json:"data"`
	}
	getBody(r, &b)
	mg := mailgun.NewMailgun(b.Data.Domain, b.APIKey)
	m := mg.NewMessage(
		fmt.Sprintf("%s <%s>", b.Data.FromName, b.Data.FromEmail),
		b.Data.Subject,
		b.Data.Body,
		b.Data.ToEmail,
	)
	if b.Data.HTMLBody != "" {
		var buffer bytes.Buffer
		tmpl, err := getTemplate("emailtemplate")
		if err != nil {
			renderJSON(w, http.StatusInternalServerError, err)
			return
		}
		if err = tmpl.Execute(&buffer, nil); err != nil {
			renderJSON(w, http.StatusInternalServerError, err)
			return
		}
		m.SetHtml(fmt.Sprintf(buffer.String(), b.Data.HTMLBody))
	}
	for _, tag := range b.Data.Tags {
		m.AddTag(tag)
	}
	if _, _, err := mg.Send(r.Context(), m); err != nil {
		renderJSON(w, http.StatusFailedDependency, err)
		return
	}
	renderJSON(w, http.StatusOK, nil)
}

func (ar apiResource) template(w http.ResponseWriter, r *http.Request) {
	var buffer bytes.Buffer
	tmpl, err := getTemplate("emailtemplate")
	if err != nil {
		renderJSON(w, http.StatusInternalServerError, err)
		return
	}
	if err = tmpl.Execute(&buffer, nil); err != nil {
		renderJSON(w, http.StatusInternalServerError, err)
		return
	}
	renderJSON(w, http.StatusOK, buffer.String())
}

func renderJSON(w http.ResponseWriter, status int, data interface{}) {
	dataEnvelope := map[string]interface{}{"code": status}
	if err, ok := data.(error); ok {
		dataEnvelope["error"] = err.Error()
		dataEnvelope["success"] = false
	} else {
		dataEnvelope["data"] = data
		dataEnvelope["success"] = true
	}
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(dataEnvelope)
}

func getBody(r *http.Request, b interface{}) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()
	if err := json.Unmarshal(body, &b); err != nil {
		panic(err)
	}
}

func getDomain(listAddress string) string {
	if !strings.Contains(listAddress, "@") {
		return ""
	}
	components := strings.Split(listAddress, "@")
	return components[1]
}

func corsHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			//handle preflight in here
			w.Header().Add("Access-Control-Allow-Origin", "*")
			w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
		} else {
			h.ServeHTTP(w, r)
		}
	})
}

func getDomains(ctx context.Context, u user) []mailgun.Domain {
	mg := mailgun.NewMailgun("", u.APIKey)
	it := mg.ListDomains(&mailgun.ListOptions{Limit: 10})
	var page []mailgun.Domain
	var domains []mailgun.Domain
	for it.Next(ctx, &page) {
		domains = append(domains, page...)

	}
	if it.Err() != nil {
		log.Println(it.Err())
		return []mailgun.Domain{}
	}
	return domains
}

func getLists(ctx context.Context, domain string, u user) []mailgun.MailingList {
	mg := mailgun.NewMailgun(domain, u.APIKey)
	it := mg.ListMailingLists(&mailgun.ListOptions{Limit: 10})
	var page []mailgun.MailingList
	var lists []mailgun.MailingList
	for it.Next(ctx, &page) {
		lists = append(lists, page...)
	}
	if it.Err() != nil {
		log.Println("failed getting mailing lists", it.Err())
		return []mailgun.MailingList{}
	}
	return lists
}

func getList(domain string, u user, list string) mailgun.MailingList {
	mg := mailgun.NewMailgun(domain, u.APIKey)
	mList, err := mg.GetMailingList(context.Background(), list)
	if err != nil {
		log.Println(err)
		return mailgun.MailingList{}
	}
	return mList
}

func getMembers(ctx context.Context, u user, list string) []mailgun.Member {
	domain := getDomain(list)
	mg := mailgun.NewMailgun(domain, u.APIKey)
	it := mg.ListMembers(list, &mailgun.ListOptions{})
	var page []mailgun.Member
	var members []mailgun.Member
	for it.Next(ctx, &page) {
		members = append(members, page...)
	}
	if it.Err() != nil {
		log.Println(it.Err())
		return []mailgun.Member{}
	}
	return members
}

func getEvents(ctx context.Context, domain, list string, u user) []sEvent {
	mg := mailgun.NewMailgun(domain, u.APIKey)
	var page []mailgun.Event
	events := []sEvent{}
	now := time.Now()
	begin := now.Add(-30 * 24 * time.Hour)
	end := now
	filter := map[string]string{}
	if list != "" {
		filter["list"] = list
		// "event": "rejected OR failed",
		// "limit": "100",
		// "to": list,
		// "from": list,
	}
	it := mg.ListEvents(&mailgun.ListEventOptions{
		Compact: true,
		Begin:   begin,
		End:     end,
		Filter:  filter,
	})
	for it.Next(ctx, &page) {
		for _, e := range page {
			singleEvent, err := newSEvent(e, now)
			if err != nil {
				continue
			}
			events = append(events, singleEvent)
		}
	}
	if it.Err() != nil {
		log.Println(it.Err())
	}
	return events
}

func (ar apiResource) saveAsTemplate(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey       string `json:"apiKey"`
		Template     string `json:"template"`
		DropboxToken string `json:"dropboxToken"`
	}
	getBody(r, &b)
	if err := deleteTemplate(b.DropboxToken); err != nil {
		renderJSON(w, http.StatusInternalServerError, err)
		return
	}
	req, err := http.NewRequest("POST", "https://content.dropboxapi.com/2/files/upload", bytes.NewBuffer([]byte(b.Template)))
	if err != nil {
		renderJSON(w, http.StatusInternalServerError, err)
		return
	}
	args, err := json.Marshal(struct {
		Path           string `json:"path"`
		Mode           string `json:"mode"`
		Autorename     bool   `json:"autorename"`
		Mute           bool   `json:"mute"`
		StrictConflict bool   `json:"strict_conflict"`
	}{
		Path:           "/template.txt",
		Mode:           "add",
		Autorename:     true,
		Mute:           false,
		StrictConflict: false,
	})
	req.Header.Add("Authorization", "Bearer "+b.DropboxToken)
	req.Header.Add("Content-Type", "application/octet-stream")
	req.Header.Add("Dropbox-API-Arg", string(args))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		renderJSON(w, http.StatusInternalServerError, err)
		return
	}
	defer resp.Body.Close()
	rr, err := ioutil.ReadAll(resp.Body)
	fmt.Println(string(rr), err)
	renderJSON(w, http.StatusOK, nil)
}

func (ar apiResource) loadTemplate(w http.ResponseWriter, r *http.Request) {
	var b struct {
		APIKey       string `json:"apiKey"`
		DropboxToken string `json:"dropboxToken"`
	}
	getBody(r, &b)
	req, err := http.NewRequest("POST", "https://content.dropboxapi.com/2/files/download", nil)
	if err != nil {
		renderJSON(w, http.StatusInternalServerError, err)
		return
	}
	args, err := json.Marshal(struct {
		Path string `json:"path"`
	}{
		Path: "/template.txt",
	})
	req.Header.Add("Authorization", "Bearer "+b.DropboxToken)
	req.Header.Add("Dropbox-API-Arg", string(args))
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		renderJSON(w, http.StatusInternalServerError, err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		renderJSON(w, http.StatusInternalServerError, err)
		return
	}
	renderJSON(w, http.StatusOK, string(body))
}

func deleteTemplate(dropboxToken string) error {
	args, err := json.Marshal(struct {
		Path string `json:"path"`
	}{
		Path: "/template.txt",
	})
	req, err := http.NewRequest("POST", "https://api.dropboxapi.com/2/files/delete_v2", bytes.NewBuffer(args))
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+dropboxToken)
	req.Header.Add("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	resp.Body.Close()
	return nil
}

func authentication(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var b struct {
			APIKey string `json:"apiKey"`
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			renderJSON(w, http.StatusBadRequest, err)
			return
		}
		r.Body.Close()
		if err := json.Unmarshal(body, &b); err != nil {
			renderJSON(w, http.StatusBadRequest, err)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(body))
		if b.APIKey == "" {
			renderJSON(w, http.StatusBadRequest, errors.New("apiKey is empty"))
			return
		}
		next.ServeHTTP(w, r)
	})
}
