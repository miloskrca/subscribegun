package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"golang.org/x/oauth2"
)

type dropboxResource struct {
	dropboxURL string
	conf       *oauth2.Config
}

func newDropboxResource() dropboxResource {
	conf := &oauth2.Config{
		ClientID:     "sbnck2yjapduwoy",
		ClientSecret: "mkbf78xamkh7e57",
		Scopes:       []string{},
		RedirectURL:  "https://subscribegun.herokuapp.com/api/oauth/redirect",
		Endpoint: oauth2.Endpoint{
			AuthURL:  "https://www.dropbox.com/oauth2/authorize",
			TokenURL: "https://api.dropboxapi.com/oauth2/token",
		},
	}

	return dropboxResource{
		conf:       conf,
		dropboxURL: conf.AuthCodeURL("state"),
	}
}

func (d dropboxResource) routes() chi.Router {
	r := chi.NewRouter()
	r.Get("/connect", d.connect)
	r.Get("/redirect", d.redirect)
	return r
}

func (d dropboxResource) connect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, d.dropboxURL, http.StatusSeeOther)
}

func (d dropboxResource) redirect(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.URL.Query())
	// Use the authorization code that is pushed to the redirect
	// URL. Exchange will do the handshake to retrieve the
	// initial access token. The HTTP Client returned by
	// conf.Client will refresh the token as necessary.
	code := r.URL.Query().Get("code")
	ctx := context.Background()
	tok, err := d.conf.Exchange(ctx, code)
	if err != nil {
		log.Fatalf("cannot exchange code: %v", err)
	}
	fmt.Println(tok)
	http.Redirect(w, r, "/dashboard?token="+tok.AccessToken, http.StatusSeeOther)
}
