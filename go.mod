module gitlab.com/miloskrca/subscribegun

go 1.12

require (
	github.com/avct/uasurfer v0.0.0-20191028135549-26b5daa857f1
	github.com/boltdb/bolt v1.3.1
	github.com/facebookgo/ensure v0.0.0-20200202191622-63f1cf65ac4c // indirect
	github.com/facebookgo/subset v0.0.0-20200203212716-c811ad88dec4 // indirect
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mailgun/mailgun-go/v4 v4.6.1
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
